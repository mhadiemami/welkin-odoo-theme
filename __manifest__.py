{
    # Theme information
    'name': "Welkin",
    'description': """
    """,
    'category': 'Theme',
    'version': '1.0',
    'depends': ['website'],

    # templates
    'data': [
        'views/options.xml',
        'views/snippets.xml',
    ],

    # demo pages
    'demo': [
        'demo/pages.xml',
    ],

    # Your information
    'author': "Mohammad Hadi Emami",
    'website': "",
}